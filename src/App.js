import React from 'react';
import { normalize } from 'polished'
import { createGlobalStyle, ThemeProvider } from 'styled-components';
import { theme } from './common/theme';
import ShoppingList from './screens/ShoppingList';

const GlobalStyles = createGlobalStyle`
  
  ${normalize()}

  html {
     font-family: ${p => p.theme.primaryFont} !important;
     color: ${p => p.theme.primaryColor};
  }
  
  body {
     background-color: ${p => p.theme.backgroundColor};
  }
 
`;

const CustomThemeProvider = ({ children }) => (
  <ThemeProvider theme={theme}>
    <>
      <GlobalStyles />
      {children}
    </>
  </ThemeProvider>
)

function App() {
  return (
    <CustomThemeProvider>
      <ShoppingList />
    </CustomThemeProvider>

  );
}

export default App;
