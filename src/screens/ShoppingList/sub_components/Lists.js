import React, { Component } from 'react';
import styled from 'styled-components';
import { string, number, arrayOf, shape, func } from 'prop-types';
import { AmountField, NameField, Form, Button } from '../../../components/Inputs';

const ItemWrapper = styled.div`
  padding: 30px 0;
  height: 50px;
`;

const Title = styled.h1`
  border-bottom: 3px solid ${p => p.theme.borderColor};
  text-align: center;
`;

class ListItem extends Component {

  shouldComponentUpdate(nextProps) {
    return nextProps.name !== this.props.name || nextProps.amount !== this.props.amount;
  }

  onKeyPress = (event) => {
    if (event.which === 13) {
      event.preventDefault();
    }
  }

  render() {
    const { index, updateProduct, removeProduct, name, amount, id } = this.props;
    return (
      <ItemWrapper key={index}>
        <Form onSubmit={n => n} onKeyPress={this.onKeyPress}>
          <NameField validators={[`minStringLength:1`]} value={name} onChange={name => updateProduct(index, { name })}/>
          <AmountField value={amount} onChange={amount => updateProduct(index, { amount })}/>
          <Button onClick={() => removeProduct(id)}>remove</Button>
        </Form>
      </ItemWrapper>
    );
  }
}

export default class Lists extends Component {

  render() {
    const { products } = this.props;
    return (
      <>
        <Title>Product lists</Title>
        {
          products.map((product, index) => {
            return (<ListItem key={index} id={product.id} name={product.name} amount={product.amount} index={index} {...this.props}/>);
          })
        }
      </>

    );
  }
}

ListItem.propTypes = {
  index: number.isRequired,
  updateProduct: func.isRequired,
  removeProduct: func.isRequired,
  name: string,
  amount: number,
  id: string
};

Lists.propTypes = {
  products: arrayOf(
    shape({
      name: string,
      amount: number,
      id: string,
    }),
  ).isRequired,
  updateProduct: func.isRequired,
  removeProduct: func.isRequired,
};
