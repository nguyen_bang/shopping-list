import React, { Component } from 'react';
import styled from 'styled-components';
import { func } from 'prop-types';
import { AmountField, NameField, Form, Button } from '../../../components/Inputs';

import faker from 'faker';

const Wrapper = styled.div`
  padding: 30px 0;
  height: 50px;
`;

const Title = styled.h1`
  border-bottom: 3px solid ${p => p.theme.borderColor};
`;

export default class AddProduct extends Component {
  initialState =  {
    name: '',
    amount: 1,
  };

  state = this.initialState;

  handleAddProduct = (event) => {
    let product = {...this.state};
    product.id = faker.random.uuid();
    this.props.addProduct(product);
    this.setState(this.initialState);
  };

  shouldComponentUpdate(nextProps, nextState) {
    return nextState.amount !== this.state.amount || nextState.name !== this.state.name;
  }

  render() {
    const { name, amount } = this.state;
    return (
      <Wrapper>
        <Title>Add new product</Title>
        <Form onSubmit={this.handleAddProduct}>
          <NameField value={name} onChange={name => this.setState({ name})}/>
          <AmountField value={amount} onChange={amount => this.setState({ amount})}/>
          <Button type="submit">add</Button>
        </Form>
      </Wrapper>
    );
  }
}

AddProduct.propTypes = {
  addProduct: func.isRequired,
};
