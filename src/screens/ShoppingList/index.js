import React, { Component } from 'react';
import styled from 'styled-components';
import { generateRandomProducts } from '../../utils';
import AddProduct from './sub_components/AddProduct';
import Lists from './sub_components/Lists';
import logo from './logo.png';

const Wrapper = styled.div`
  margin: 100px auto;
  max-width: 1040px;
`;

export const TitileWrapper = styled.div`
  display: flex;
  align-items: center;
`;

const Image = styled.img`
  width: 100px;
`;

export const Title = styled.h1`
  margin-left: 20px;
`;

export const ListWrapper = styled.div``;

export const AddProductWrapper = styled.div`
  padding: 20px 0;
`;

export default class ShoppingList extends Component {
  state = {
    products: generateRandomProducts(),
  };

  updateProduct = (index, updatedProperty) => {
    const products = [...this.state.products];
    products[index] = { ...products[index], ...updatedProperty };
    this.setState({ products });
  };

  addProduct = product => {
    const { products } = this.state;
    this.setState({ products: products.concat(product) });
  };

  removeProduct = id => {
    const products = this.state.products.filter(product => product.id !== id);
    this.setState({ products });
  };

  render() {
    const { products } = this.state;
    return (
      <Wrapper>
        <TitileWrapper>
          <Image src={logo} alt={'logo of company'} />
          <Title>Shopping List</Title>
        </TitileWrapper>
        <ListWrapper>
          <Lists updateProduct={this.updateProduct} products={products} removeProduct={this.removeProduct} />
        </ListWrapper>
        <AddProductWrapper>
          <AddProduct addProduct={this.addProduct} />
        </AddProductWrapper>
      </Wrapper>
    );
  }
}
