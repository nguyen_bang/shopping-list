import React from 'react';
import { AMOUNT } from '../utils';
import { TextValidator, ValidatorForm } from 'react-material-ui-form-validator';
import styled from 'styled-components';
const validateName = { validators: ['required'], message: ['this field is required'] };

const validateNumber = {
  validators: [`minNumber:${AMOUNT.MIN}`, `maxNumber:${AMOUNT.MAX}`],
  message: ['number is too low', 'number is too high'],
};

export const NameField = ({ value, onChange, style, ...props }) => {
  return (
    <TextValidator style={{ width: '60%', ...style }}
                   label="Name" name="name"
                   type={'text'}
                   value={value}
                   onChange={event => onChange(event.target.value)}
                   validators={validateName.validators}
                   errorMessages={validateName.message} {...props}/>
  );
};

export const AmountField = ({ value, onChange, style, ...props }) => {
  return (
    <TextValidator
      style={{ width: '25%', margin: '0 40px', ...style }}
      label="Amount"
      name="amount"
      type={'number'}
      value={value}
      onChange={event => {
        let value = event.target.value;
        onChange(Number(value) || 1)
      }}
      validators={validateNumber.validators} errorMessages={validateNumber.message} {...props}/>
  );
};

export const Button = styled.button`
  width: 10%;
  border-radius: 10px;
  padding: 10px 20px;
  background-color:${p => p.theme.buttonBackground};
  cursor: pointer;
`

export const Form = styled(ValidatorForm)`
  width: 100%;
  display: flex;
  justify-content: space-between;
  align-items: center;
  height: 100%;
`
