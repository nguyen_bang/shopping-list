import random from 'lodash/random';
import faker from 'faker';

export const AMOUNT = {
  MIN: 1,
  MAX: 99,
};


export function generateRandomProducts() {
  let products = Array.from(Array(random(5, 15)).keys());
  return products.map(() => ({
    id: faker.random.uuid(),
    name: faker.commerce.productName(),
    amount: random(AMOUNT.MIN, AMOUNT.MAX),
  }));
}
